use std::sync::mpsc::{channel, Receiver, Sender, SendError};
use std::thread::{JoinHandle, spawn};

/// Services can only receive requests from an implementation of this trait.
///
/// # Example
///
/// ```
/// # use momy_core::generic_service::ServiceRequest;
/// pub enum SomeRequest { DoSomething, Close }
///
/// impl ServiceRequest for SomeRequest {
///     fn close() -> Self {
///         Self::Close
///     }
///
///     fn is_close(&self) -> bool {
///         match self {
///             Self::Close => true,
///             _ => false,
///         }
///     }
/// }
///
/// // the following assertion should always be true for any implementation
/// assert!(SomeRequest::close().is_close())
/// ```
pub trait ServiceRequest: Send {
    /// This method is used by the service handle to send a request to close the channel
    /// to the service.
    fn close() -> Self;
    /// This method allows the Service to know if the channel needs to be closed
    /// with this request. This method should not have any side-effect.
    fn is_close(&self) -> bool; // TODO: make this method const when const fn is stable
}

pub trait Service {
    type Request: ServiceRequest + Clone + Send;
    type ServiceError;

    /// Services that implements this trait will receive all incoming requests
    /// through this method. Note that this method will never be called with
    /// any objects that would return.
    fn execute(&mut self, request: Self::Request);

    /// Sends the given result into the sender. If the result cannot be sent,
    /// it returns the value wrapped into an option.
    fn send_result<U>(
        &self,
        result: Result<U, Self::ServiceError>,
        sender: Sender<Result<U, Self::ServiceError>>
    ) -> Option<U> {
        sender.send(result)
            .map_err(|error| error.0).err()
            .and_then(|result| result.ok())
    }

    /// Similar to `send_result`, but when the fact that the result could not
    /// be sent does not need to be handled.
    fn send_and_forget<U>(
        &self,
        result: Result<U, Self::ServiceError>,
        sender: Sender<Result<U, Self::ServiceError>>
    ) {
        if let Some(_) = self.send_result(result, sender) {
            // TODO: log that channel error
        }
    }
}

#[derive(Clone)]
pub struct RequestSender<T: Clone + Send> {
    sender: Sender<T>,
}

impl<T: Clone + Send> RequestSender<T> {
    pub fn new(sender: Sender<T>) -> Self {
        Self {
            sender,
        }
    }

    pub fn send(&self, request: T) -> Result<(), SendError<T>> {
        self.sender.send(request)
    }

    pub fn send_request(
        &self,
        request: T,
    ) -> Result<(), SendError<T>> {
        self.sender.send(request)
    }

    pub fn send_request_with_result<A, B>(
        &self,
        request: T,
        receiver: Receiver<Result<A, B>>
    ) -> Result<A, ServiceHandleError<B>> {
        self.send_request(request)
            .map_err(|_| ServiceHandleError::TranmissionError)?;

        receiver.recv()
            .map_err(|_| ServiceHandleError::TranmissionError)?
            .map_err(|e| ServiceHandleError::ServiceError(e))
    }
}

#[derive(Debug)]
pub enum ServiceHandleError<T> {
    TranmissionError,
    ServiceError(T),
}

pub struct ServiceHandle<T: Service> {
    sender: RequestSender<T::Request>,
    handle: Option<JoinHandle<()>>,
}

impl<T: Service> ServiceHandle<T> {
    pub fn join(&mut self) {
        if let Some(handle) = self.handle.take() {
            self.sender.send(T::Request::close()).unwrap();
            handle.join().unwrap();
        };
    }

    pub fn get_sender(&self) -> &RequestSender<T::Request> {
        &self.sender
    }
}

pub fn spawn_service<T: Service + Send + 'static>(mut service: T) -> ServiceHandle<T> {
    let (sender, receiver): (Sender<T::Request>, Receiver<T::Request>) = channel();

    let handle = spawn(move || {
        loop {
            match receiver.recv() {
                Ok(request) => if request.is_close() {
                    break
                } else {
                    service.execute(request);
                },
                Err(_) => break,
            };
        }
    });

    ServiceHandle {
        sender: RequestSender::new(sender),
        handle: Some(handle),
    }
}
