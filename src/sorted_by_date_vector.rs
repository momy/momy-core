use chrono::NaiveDate;
use std::iter::Iterator;

pub struct SortedByDateVector<T> {
    elements: Vec<(T, NaiveDate)>,
}

impl<T> SortedByDateVector<T> {
    pub fn new(element: T, date: NaiveDate) -> Self {
        Self {
            elements: vec![(element, date)],
        }
    }

    pub fn insert(&mut self, element: T, date: NaiveDate) {
        let result_found = self.elements.iter().enumerate()
            .find(|(_, (_, element_date))| element_date >= &date)
            .map_or_else(|| None, |item| Some(item));

        if let Some((index, (_, existing_date))) = result_found {
            if &date == existing_date {
                self.elements[index] = (element, date);
            } else {
                self.elements.insert(index, (element, date));
            }
        } else {
            self.elements.push((element, date));
        }
    }

    pub fn get_from_date(&self, date: &NaiveDate) -> Option<&T> {
        let mut iterator = self.elements.iter().enumerate();

        match iterator.find(|(_, (_, element_date))| date < &element_date) {
            Some(index_element_date) => match index_element_date {
                (index, (element, element_date)) => if element_date == date {
                    Some(&element)
                } else {
                    Some(&self.elements[index.checked_sub(1)?].0)
                }
            },
            None => self.elements.last().map(|(element, _)| element)
        }
    }

    pub fn last(&self) -> &(T, NaiveDate) {
        self.elements.last().expect("this collection cannot be empty")
    }

    pub fn len(&self) -> usize {
        self.elements.len()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn returns_none_when_date_is_before_element_date() {
        let early_date = NaiveDate::from_ymd(2017, 6, 1);
        let later_date = NaiveDate::from_ymd(2018, 6, 1);

        let vector = SortedByDateVector::new(0, later_date);
        assert!(vector.get_from_date(&early_date).is_none());
    }

    #[test]
    fn returns_element_when_date_is_equal_to_element_date() {
        let date = NaiveDate::from_ymd(2017, 6, 1);
        let same_date = date.clone();
        let element = 0;

        let vector = SortedByDateVector::new(element, date);

        assert_eq!(vector.get_from_date(&same_date).unwrap(), &element);
    }

    #[test]
    fn returns_element_when_date_is_after_element_date() {
        let early_date = NaiveDate::from_ymd(2017, 6, 1);
        let later_date = NaiveDate::from_ymd(2018, 6, 1);
        let element = 0;

        let vector = SortedByDateVector::new(element, early_date);

        assert_eq!(vector.get_from_date(&later_date).unwrap(), &element);
    }

    #[test]
    fn returns_element_when_date_is_after_added_element() {
        let early_date = NaiveDate::from_ymd(2017, 6, 1);
        let later_date = NaiveDate::from_ymd(2018, 6, 1);
        let after_later_date = NaiveDate::from_ymd(2019, 6, 1);
        let added_element = 7;

        let mut vector = SortedByDateVector::new(0, early_date);
        vector.insert(added_element, later_date);

        assert_eq!(vector.get_from_date(&after_later_date).unwrap(), &added_element);
    }

    #[test]
    fn returns_element_before_the_given_date() {
        let early_date = NaiveDate::from_ymd(2017, 6, 1);
        let later_date = NaiveDate::from_ymd(2018, 6, 1);
        let after_later_date = NaiveDate::from_ymd(2019, 6, 1);
        let element = 7;

        let mut vector = SortedByDateVector::new(element, early_date);
        vector.insert(7, after_later_date);

        assert_eq!(vector.get_from_date(&later_date).unwrap(), &element);
    }

    #[test]
    fn returns_element_at_the_given_date() {
        let early_date = NaiveDate::from_ymd(2017, 6, 1);
        let later_date = NaiveDate::from_ymd(2018, 6, 1);
        let new_element = 7;

        let mut vector = SortedByDateVector::new(0, early_date);
        vector.insert(new_element, later_date);

        assert_eq!(vector.get_from_date(&later_date).unwrap(), &new_element);
    }

    #[test]
    fn inserts_element_at_same_date_replace_existing_one() {
        let early_date = NaiveDate::from_ymd(2017, 6, 1);
        let later_date = NaiveDate::from_ymd(2018, 6, 1);
        let element = 1;
        let new_element = 7;

        let mut vector = SortedByDateVector::new(0, early_date);
        vector.insert(element, later_date);
        vector.insert(new_element, later_date);

        assert_eq!(vector.len(), 2);
        assert_eq!(vector.get_from_date(&later_date).unwrap(), &new_element);
    }
}
