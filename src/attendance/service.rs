use crate::attendance::{
    Attendance,
    Persistence,
    PersistenceError,
    Request,
    DaySchedule,
    ExpectedAttendance,
};
use crate::child;
use crate::child::{
    ChildId,
    ServiceResult as ChildServiceResult,
};
use crate::generic_service::{RequestSender, Service as GenericService};

use chrono::{Datelike, NaiveDate};
use std::collections::HashMap;
use std::sync::mpsc::{Receiver, SendError};

#[derive(Debug, Eq, PartialEq)]
pub enum ServiceError {
    ChildNotFound,
    ChildNotRegistered,
    RegularSchoolWeekAttendanceNotFound,
    ChildServiceFailed,
    ChildServiceChannelFailed,
    PersistenceError,
}

pub type Result<T> = std::result::Result<T, ServiceError>;

impl From<PersistenceError> for ServiceError {
    fn from(error: PersistenceError) -> ServiceError {
        match error {
            PersistenceError::RegularSchoolWeekAttendanceNotFound
                 => Self::RegularSchoolWeekAttendanceNotFound,
            PersistenceError::InternalError => ServiceError::PersistenceError,
        }
    }
}

impl From<child::ServiceError> for ServiceError {
    fn from(error: child::ServiceError) -> ServiceError {
        match error {
            child::ServiceError::AlreadyRegistered
            | child::ServiceError::PersistenceError
            | child::ServiceError::CannotRegisterInThePast => Self::ChildServiceFailed,
            child::ServiceError::ChildNotFound => Self::ChildNotFound,
            child::ServiceError::NotRegistered => Self::ChildNotRegistered,
        }
    }
}

fn convert_error<T: Into<ServiceError>>(error: T) -> ServiceError {
    error.into()
}

pub struct Service<T, U> where
    T: Persistence<U>,
    U: ChildId
{
    persistence: T,
    child_service: Option<RequestSender<child::Request<U>>>,
}

impl<T, U> Service<T, U> where
    T: Persistence<U>,
    U: ChildId
{
    pub fn new(persistence: T) -> Service<T, U> {
        Service {
            persistence,
            child_service: None,
        }
    }

    pub fn set_child_service_sender(&mut self, sender: RequestSender<child::Request<U>>) {
        self.child_service.replace(sender);
    }

    pub fn set_attendance(&mut self, child_id: U, date: NaiveDate, attendance: Attendance) -> Result<()> {
        let result = self.send_request_to_child_service_and_receive(
            child::Request::is_registered(child_id, date.clone())
        )?;

        result.map_err(convert_error)
            .and_then(|is_registered| {
                if is_registered {
                    self.persistence.set_attendance(date, child_id, attendance)
                        .map_err(convert_error)
                } else {
                    Err(ServiceError::ChildNotRegistered)
                }
            })
    }

    pub fn get_attendance(&self, date: &NaiveDate) -> Result<HashMap<U, Attendance>> {
        self.persistence.get_attendance(date)
            .map_err(convert_error)
    }

    pub fn set_schedule(&mut self, days: Vec<(NaiveDate, DaySchedule)>) -> Result<()> {
        self.persistence.set_schedule(days)
            .map_err(convert_error)
    }

    pub fn get_schedule(&self, from: &NaiveDate, to: &NaiveDate) -> Result<Vec<(NaiveDate, DaySchedule)>> {
        self.persistence.get_schedule(from, to)
            .map_err(convert_error)
    }

    pub fn set_expected_attendance(&self, _date: &NaiveDate, _child_id: U) -> Result<()> {
        // TODO
        Ok(())
    }

    pub fn get_expected_children(&self, date: &NaiveDate) -> Result<HashMap<U, ExpectedAttendance>> {
        match self.persistence.get_day_schedule(date).map_err(convert_error)? {
            DaySchedule::Closed => Ok(HashMap::new()),
            DaySchedule::School => self.get_school_attendances(date),
            DaySchedule::FullDay => Ok(HashMap::new()), // TODO
        }
    }

    fn get_school_attendances(&self, date: &NaiveDate) -> Result<HashMap<U, ExpectedAttendance>> {
        let child_ids = self.send_request_to_child_service_and_receive(
            child::Request::get_registered_children(date.clone())
        )?
            .map_err(convert_error)?;

        child_ids.iter()
            .map(|child_id| self.get_school_attendance(date, *child_id)
                .map(|attendance| (*child_id, attendance)))
            .collect()
    }

    fn get_school_attendance(&self, date: &NaiveDate, child_id: U) -> Result<ExpectedAttendance> {
        let week = self.persistence.get_expected_school_attendance(date, child_id)
            .map_err(convert_error)?;
        let school_attendance = week.get_expected_attendance(&date.weekday()).clone();

        Ok(ExpectedAttendance::SchoolDay(school_attendance))
    }

    fn send_request_to_child_service(
        &self,
        request: child::Request<U>
    ) -> std::result::Result<(), SendError<child::Request<U>>> {
        self.child_service.as_ref()
            .expect("Child Service sender not registered in Attendance Service")
            .send(request)
    }

    fn send_request_to_child_service_and_receive<V>(
        &self,
        (request, receiver): (child::Request<U>, Receiver<ChildServiceResult<V>>),
    ) -> Result<ChildServiceResult<V>> {
        self.send_request_to_child_service(request)
            .map_err(|_| ServiceError::ChildServiceChannelFailed)?;

        receiver.recv().map_err(|_| ServiceError::ChildServiceChannelFailed)
    }
}

impl<T, U> GenericService for Service<T, U> where
    T: Persistence<U>,
    U: ChildId
{
    type Request = Request<U>;
    type ServiceError = ServiceError;

    fn execute(&mut self, request: Self::Request) {
        match request {
            Request::SetAttendance(date, child_id, attendance, sender) => {
                let result = self.set_attendance(child_id, date, attendance);
                self.send_and_forget(result, sender);
            }
            Request::GetScheduleBetween(start_date, end_date, sender) => {
                let result = self.get_schedule(&start_date, &end_date);
                self.send_and_forget(result, sender);
            }
            Request::SetSchedule(schedules, sender) => {
                let result = self.set_schedule(schedules);
                self.send_and_forget(result, sender);
            }
            Request::SetChildRequestSender(sender) => {
                self.set_child_service_sender(sender);
            }
            Request::Close => (),
        };
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::attendance::MemoryAttendancePersistence;
    use crate::child;
    use crate::child::{Child, MemoryChildPersistence};
    use crate::generic_service::{ServiceHandle, spawn_service};

    fn get_service() -> (Service<MemoryAttendancePersistence, u32>,
        ServiceHandle<child::Service<MemoryChildPersistence, u32>>
    ) {
        let child_handle = spawn_service(child::Service::new(MemoryChildPersistence::new()));
        let mut service = Service::new(MemoryAttendancePersistence::new());
        service.set_child_service_sender(child_handle.get_sender().clone());

        (service, child_handle)
    }

    fn some_date() -> NaiveDate {
        NaiveDate::from_ymd(2018, 1, 1)
    }

    #[test]
    fn set_attendance_of_unknown_child_id_should_return_child_not_found_error() {
        let (mut service, _) = get_service();
        let date = some_date();

        assert_eq!(
            ServiceError::ChildNotFound,
            service.set_attendance(0, date, Attendance::any()).unwrap_err()
        );
    }

    #[test]
    fn set_attendance_of_unregistered_child_id_should_return_child_not_registered_error() {
        let (mut service, child_handle) = get_service();
        let date = some_date();

        let (request, receiver) = child::Request::create_child(Child::any());
        let child_id = child_handle.get_sender()
            .send_request_with_result(request, receiver).unwrap();

        assert_eq!(
            ServiceError::ChildNotRegistered,
            service.set_attendance(child_id, date, Attendance::any()).unwrap_err()
        );
    }

    #[test]
    fn default_schedule_should_be_closed() {
        let (service, _) = get_service();
        let date = some_date();

        assert_eq!(
            vec![(date, DaySchedule::Closed)],
            service.get_schedule(&date, &date).unwrap()
        );
    }

    #[test]
    fn set_schedule_should_persist() {
        let (mut service, _) = get_service();
        let date = some_date();

        service.set_schedule(vec![(date.clone(), DaySchedule::School)]).unwrap();

        assert_eq!(
            vec![(date, DaySchedule::School)],
            service.get_schedule(&date, &date).unwrap()
        );
    }

    #[test]
    fn change_schedule_should_persist() {
        let (mut service, _) = get_service();
        let date = some_date();

        service.set_schedule(vec![(date.clone(), DaySchedule::School)]).unwrap();
        service.set_schedule(vec![(date.clone(), DaySchedule::FullDay)]).unwrap();

        assert_eq!(
            vec![(date, DaySchedule::FullDay)],
            service.get_schedule(&date, &date).unwrap()
        );
    }
}
