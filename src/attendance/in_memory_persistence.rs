use crate::attendance::{
    Attendance,
    Persistence,
    PersistenceError,
    DaySchedule,
    ExpectedAttendance,
    PersistenceResult,
    RegularSchoolWeekAttendance,
};
use crate::sorted_by_date_vector::SortedByDateVector;

use chrono::NaiveDate;
use std::collections::HashMap;

type Result<T> = PersistenceResult<T>;
type DateMap<T> = HashMap<NaiveDate, T>;

pub struct MemoryAttendancePersistence {
    attendances: DateMap<HashMap<u32, Attendance>>,
    schedules: DateMap<DaySchedule>,
    expected_attendances: DateMap<HashMap<u32, ExpectedAttendance>>,
    expected_school_attendances: HashMap<u32, SortedByDateVector<RegularSchoolWeekAttendance>>,
}

impl MemoryAttendancePersistence {
    pub fn new() -> Self {
        Self {
            attendances: HashMap::new(),
            schedules: HashMap::new(),
            expected_attendances: HashMap::new(),
            expected_school_attendances: HashMap::new(),
        }
    }
}

impl Persistence<u32> for MemoryAttendancePersistence {
    fn set_attendance(&mut self, date: NaiveDate, child_id: u32, attendance: Attendance) -> Result<()> {
        if let Some(child_id_map) = self.attendances.get_mut(&date) {
            child_id_map.insert(child_id, attendance);
        } else {
            let mut child_id_map = HashMap::new();
            child_id_map.insert(child_id, attendance);
            self.attendances.insert(date, child_id_map);
        };

        Ok(())
    }

    fn get_attendance(&self, date: &NaiveDate) -> Result<HashMap<u32, Attendance>> {
        Ok(match self.attendances.get(date) {
            Some(attendances) => attendances.clone(),
            None => HashMap::new(),
        })
    }

    fn set_day_schedule(&mut self, date: NaiveDate, schedule: DaySchedule) -> Result<()> {
        match schedule {
            DaySchedule::Closed => self.schedules.remove(&date),
            _ => self.schedules.insert(date, schedule),
        };
        Ok(())
    }

    fn get_day_schedule(&self, date: &NaiveDate) -> Result<DaySchedule> {
        Ok(self.schedules.get(date).cloned().unwrap_or(DaySchedule::Closed))
    }

    fn set_expected_school_attendance(
        &mut self,
        date: NaiveDate,
        child_id: u32,
        attendance: RegularSchoolWeekAttendance,
    ) -> Result<()> {
        if let Some(expected_school) = self.expected_school_attendances.get_mut(&child_id) {
            expected_school.insert(attendance, date);
        } else {
            self.expected_school_attendances.insert(child_id, SortedByDateVector::new(attendance, date));
        };

        Ok(())
    }

    fn get_expected_school_attendance(
        &self,
        date: &NaiveDate,
        child_id: u32,
    ) -> Result<RegularSchoolWeekAttendance> {
        match self.expected_school_attendances.get(&child_id) {
            Some(expected_school) => expected_school.get_from_date(date).cloned()
                .ok_or(PersistenceError::RegularSchoolWeekAttendanceNotFound),
            None => Err(PersistenceError::RegularSchoolWeekAttendanceNotFound),
        }
    }

    fn get_expected_attendance(&mut self, date: &NaiveDate) -> Result<HashMap<u32, ExpectedAttendance>> {
        Ok(match self.expected_attendances.get(date) {
            Some(expected) => expected.clone(),
            None => HashMap::new(),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::attendance::SchoolDayAttendance;

    fn subject() -> MemoryAttendancePersistence {
        MemoryAttendancePersistence::new()
    }

    #[test]
    fn get_attendance_should_map_child_id_to_attendance() {
        let attendance = Attendance::SchoolDay(SchoolDayAttendance::absent(), 0);
        let child_id = 1;
        let date = NaiveDate::from_ymd(2017, 5, 14);
        let mut persistence = subject();

        persistence.set_attendance(date.clone(), child_id, attendance.clone()).unwrap();

        let attendances = persistence.get_attendance(&date).unwrap();

        assert_eq!(attendances.len(), 1);
        assert_eq!(attendances.get(&child_id).unwrap(), &attendance);
    }

    #[test]
    fn get_attendance_should_return_empty_map_if_no_attendance_at_date() {
        let attendance = Attendance::SchoolDay(SchoolDayAttendance::absent(), 0);
        let child_id = 1;
        let date = NaiveDate::from_ymd(2017, 5, 14);
        let other_date = NaiveDate::from_ymd(2018, 5, 14);
        let mut persistence = subject();

        persistence.set_attendance(date.clone(), child_id, attendance.clone()).unwrap();

        let attendances = persistence.get_attendance(&other_date).unwrap();

        assert_eq!(attendances.len(), 0);
    }

    #[test]
    fn get_day_schedule_should_return_persisted_schedule() {
        let schedule = DaySchedule::School;
        let date = NaiveDate::from_ymd(2017, 5, 14);
        let mut persistence = subject();

        persistence.set_day_schedule(date.clone(), schedule.clone()).unwrap();

        let persisted_schedule = persistence.get_day_schedule(&date).unwrap();

        assert_eq!(schedule, persisted_schedule);
    }

    #[test]
    fn set_multiple_schedule_should_persist() {
        let schedule = DaySchedule::School;
        let date = NaiveDate::from_ymd(2017, 5, 14);
        let mut persistence = subject();

        persistence.set_schedule(vec!((date.clone(), schedule.clone()))).unwrap();

        let persisted_schedule = persistence.get_day_schedule(&date).unwrap();

        assert_eq!(schedule, persisted_schedule);
    }
}
