use crate::attendance::{Attendance, DaySchedule, ServiceResult};
use crate::child;
use crate::generic_service::{RequestSender, ServiceRequest};

use chrono::NaiveDate;
use std::sync::mpsc::{channel, Receiver, Sender};

type ResultSender<T> = Sender<ServiceResult<T>>;
type ResultReceiver<T> = Receiver<ServiceResult<T>>;

#[derive(Clone)]
pub enum Request<ChildId: Clone + Send> {
    SetAttendance(NaiveDate, ChildId, Attendance, ResultSender<()>),
    GetScheduleBetween(NaiveDate, NaiveDate, ResultSender<Vec<(NaiveDate, DaySchedule)>>),
    SetSchedule(Vec<(NaiveDate, DaySchedule)>, ResultSender<()>),
    SetChildRequestSender(RequestSender<child::Request<ChildId>>),
    Close,
}

impl<ChildId: Clone + Send> Request<ChildId> {
    pub fn set_attendance(
        date: NaiveDate,
        child_id: ChildId,
        attendance: Attendance
    ) -> (Self, ResultReceiver<()>) {
        let (sender, receiver) = channel();

        (Self::SetAttendance(date.into(), child_id, attendance, sender), receiver)
    }

    pub fn get_schedule_between(start_date: NaiveDate, end_date: NaiveDate) -> (Self, ResultReceiver<Vec<(NaiveDate, DaySchedule)>>) {
        let (sender, receiver) = channel();

        (Self::GetScheduleBetween(start_date, end_date, sender), receiver)
    }

    pub fn set_schedule(schedules: Vec<(NaiveDate, DaySchedule)>) -> (Self, ResultReceiver<()>) {
        let (sender, receiver) = channel();

        (Self::SetSchedule(schedules, sender), receiver)
    }

    pub fn set_child_request_sender(sender: RequestSender<child::Request<ChildId>>) -> Self {
        Self::SetChildRequestSender(sender)
    }

    pub fn close() -> Self {
        Self::Close
    }
}

impl<ChildId: Clone + Send> ServiceRequest for Request<ChildId> {
    fn close() -> Self {
        Self::Close
    }

    fn is_close(&self) -> bool {
        match self {
            Self::Close => true,
            _ => false,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    type Request = super::Request<u32>;

    #[test]
    fn is_close_is_true_for_close_provider() {
        assert!(Request::close().is_close());
    }
}
