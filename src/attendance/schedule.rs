#[derive(Clone, Debug, Eq, PartialEq)]
pub enum DaySchedule {
    Closed,
    FullDay,
    School,
}
