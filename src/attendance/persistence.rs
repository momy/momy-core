use crate::attendance::{
    Attendance,
    DaySchedule,
    ExpectedAttendance,
    RegularSchoolWeekAttendance,
};

use chrono::{Datelike, NaiveDate};
use std::collections::HashMap;

#[derive(Debug)]
pub enum PersistenceError {
    RegularSchoolWeekAttendanceNotFound,
    InternalError,
}

pub type Result<T> = std::result::Result<T, PersistenceError>;

pub trait Persistence<ChildId> {
    fn set_attendance(&mut self, date: NaiveDate, child_id: ChildId, attendance: Attendance) -> Result<()>;
    fn get_attendance(&self, date: &NaiveDate) -> Result<HashMap<ChildId, Attendance>>;
    fn set_day_schedule(&mut self, date: NaiveDate, schedule: DaySchedule) -> Result<()>;
    fn get_day_schedule(&self, date: &NaiveDate) -> Result<DaySchedule>;
    fn set_expected_school_attendance(&mut self, date: NaiveDate, child_id: ChildId, attendance: RegularSchoolWeekAttendance) -> Result<()>;
    fn get_expected_school_attendance(&self, date: &NaiveDate, child_id: ChildId) -> Result<RegularSchoolWeekAttendance>;
    fn get_expected_attendance(&mut self, date: &NaiveDate) -> Result<HashMap<ChildId, ExpectedAttendance>>;

    fn set_schedule(&mut self, schedules: Vec<(NaiveDate, DaySchedule)>) -> Result<()> {
        for (date, schedule) in schedules.into_iter() {
            self.set_day_schedule(date, schedule)?;
        }

        Ok(())
    }

    fn get_schedule(&self, from: &NaiveDate, to: &NaiveDate) -> Result<Vec<(NaiveDate, DaySchedule)>> {
        (from.num_days_from_ce()..to.num_days_from_ce() + 1)
            .map(|num_day| {
                let date = NaiveDate::from_num_days_from_ce(num_day);

                self.get_day_schedule(&date)
                    .map(|schedule| (date, schedule))
            })
            .collect()
    }
}
