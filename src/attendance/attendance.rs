use chrono::Weekday;
use std::convert::TryInto;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct SchoolDayAttendance {
    am: bool,
    lunch: bool,
    pm: bool,
}

impl SchoolDayAttendance {
    pub fn new(am: bool, lunch: bool, pm: bool) -> Self {
        SchoolDayAttendance {
            am,
            lunch,
            pm,
        }
    }

    #[cfg(test)]
    pub fn any() -> Self {
        Self::new(false, true, true)
    }

    pub fn absent() -> SchoolDayAttendance {
        SchoolDayAttendance::new(false, false, false)
    }

    pub fn is_absent(&self) -> bool {
        !(self.am || self.lunch || self.pm)
    }

    pub fn if_present_am<T>(&self, if_present: T, if_absent: T) -> T {
        if self.am { if_present } else { if_absent }
    }

    pub fn if_present_lunch<T>(&self, if_present: T, if_absent: T) -> T {
        if self.lunch { if_present } else { if_absent }
    }

    pub fn if_present_pm<T>(&self, if_present: T, if_absent: T) -> T {
        if self.pm { if_present } else { if_absent }
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct FullDayAttendance {
    present: bool,
}

impl FullDayAttendance {
    pub fn new(present: bool) -> Self {
        FullDayAttendance {
            present,
        }
    }

    pub fn if_present<T>(&self, if_present: T, if_absent: T) -> T {
        if self.present { if_present } else { if_absent }
    }
}

type OvertimeMinute = u32;

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Attendance {
    SchoolDay(SchoolDayAttendance, OvertimeMinute),
    FullDay(FullDayAttendance, OvertimeMinute),
}

impl Attendance {
    #[cfg(test)]
    pub fn any() -> Self {
        Self::SchoolDay(SchoolDayAttendance::any(), 0)
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum ExpectedAttendance {
    SchoolDay(SchoolDayAttendance),
    FullDay,
    Absent,
}

impl ExpectedAttendance {
    #[cfg(test)]
    pub fn any() -> Self {
        Self::Absent
    }
}

#[derive(Clone)]
pub struct RegularSchoolWeekAttendance {
    week_attendance: [SchoolDayAttendance; 5],
}

lazy_static! {
    static ref ABSENT: SchoolDayAttendance = SchoolDayAttendance::absent();
}

impl RegularSchoolWeekAttendance {
    pub fn new(week_attendance: [SchoolDayAttendance; 5]) -> Self {
        Self {
            week_attendance,
        }
    }

    pub fn get_expected_attendance(&self, day: &Weekday) -> &SchoolDayAttendance {
        // if can't cast to usize, return larger than array to return SchoolDayAttendance::Absent
        let index_from_day = day.num_days_from_monday()
            .try_into().unwrap_or(5);

        if index_from_day < 5 {
            &self.week_attendance[index_from_day]
        } else {
            &ABSENT
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_week() -> RegularSchoolWeekAttendance {
        RegularSchoolWeekAttendance::new([
            SchoolDayAttendance::absent(),
            SchoolDayAttendance::new(false, true, false),
            SchoolDayAttendance::new(false, true, false),
            SchoolDayAttendance::new(false, true, false),
            SchoolDayAttendance::new(false, true, false),
        ])
    }

    mod get_expected_attendance {
        use super::*;

        #[test]
        fn not_a_week_day_should_return_absent() {
            let attendance = get_week();

            assert_eq!(&SchoolDayAttendance::absent(), attendance.get_expected_attendance(&Weekday::Sat));
        }
    }
}
