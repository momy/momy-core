mod attendance;
mod persistence;
mod request;
mod schedule;
mod service;

#[cfg(any(test, feature = "in_memory"))]
mod in_memory_persistence;

pub use attendance::*;
pub use persistence::{
    Persistence,
    PersistenceError,
    Result as PersistenceResult
};
pub use request::Request;
pub use schedule::DaySchedule;
pub use service::{
    Service,
    ServiceError,
    Result as ServiceResult,
};

#[cfg(any(test, feature = "in_memory"))]
pub use in_memory_persistence::MemoryAttendancePersistence;
