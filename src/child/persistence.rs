use crate::child::Child;

use chrono::NaiveDate;
use std::collections::HashMap;

#[derive(Debug, Eq, PartialEq)]
pub enum PersistenceError {
    AlreadyRegistered,
    CannotRegisterInThePast,
    ChildNotFound,
    NotRegistered,
    InternalError,
}

pub type Result<T> = std::result::Result<T, PersistenceError>;

pub trait Persistence<ChildId> {
    fn create(&mut self, child: Child) -> Result<ChildId>;
    fn get_child(&self, id: ChildId) -> Result<Child>;
    fn get_all_children(&self) -> Result<HashMap<ChildId, Child>>;
    fn register(&mut self, child_id: ChildId, date: NaiveDate) -> Result<()>;
    fn unregister(&mut self, child_id: ChildId, date: NaiveDate) -> Result<()>;
    fn get_registered_children(&self, date: &NaiveDate) -> Result<Vec<ChildId>>;
    fn is_registered(&self, child_id: ChildId, date: &NaiveDate) -> Result<bool>;
}
