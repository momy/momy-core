use crate::child::{
    Child,
    ChildId,
    Persistence,
    PersistenceError,
    Request,
};
use crate::generic_service::Service as GenericService;

use chrono::NaiveDate;
use std::collections::HashMap;
use std::marker::PhantomData;

#[derive(Debug, Eq, PartialEq)]
pub enum ServiceError {
    AlreadyRegistered,
    CannotRegisterInThePast,
    ChildNotFound,
    NotRegistered,
    PersistenceError,
}

pub type Result<T> = std::result::Result<T, ServiceError>;

impl From<PersistenceError> for ServiceError {
    fn from(error: PersistenceError) -> ServiceError {
        match error {
            PersistenceError::ChildNotFound => ServiceError::ChildNotFound,
            PersistenceError::AlreadyRegistered => ServiceError::AlreadyRegistered,
            PersistenceError::CannotRegisterInThePast => ServiceError::CannotRegisterInThePast,
            PersistenceError::NotRegistered => ServiceError::NotRegistered,
            PersistenceError::InternalError => ServiceError::PersistenceError,
        }
    }
}

pub struct Service<T, U> where T: Persistence<U>, U: ChildId {
    persistence: T,
    phantom: PhantomData<U>,
}

impl<T, U> Service<T, U> where
    T: Persistence<U>,
    U: ChildId
{
    pub fn new(persistence: T) -> Self {
        Self {
            persistence,
            phantom: PhantomData,
        }
    }

    pub fn create_child(&mut self, child: Child) -> Result<U> {
        match self.persistence.create(child) {
            Ok(id) => Ok(id),
            Err(_) => Err(ServiceError::PersistenceError),
        }
    }

    pub fn get_child(&mut self, child_id: U) -> Result<Child> {
        self.persistence.get_child(child_id)
            .map_err(|error| error.into())
    }

    pub fn get_all_children(&mut self) -> Result<HashMap<U, Child>> {
        self.persistence.get_all_children()
            .map_err(|error| error.into())
    }

    pub fn get_registered_children(&mut self, date: &NaiveDate) -> Result<Vec<U>> {
        self.persistence.get_registered_children(date)
            .map_err(|error| error.into())
    }

    pub fn register_child(&mut self, child_id: U, date: NaiveDate) -> Result<()> {
        self.persistence.register(child_id, date)
            .map_err(|error| error.into())
    }

    pub fn is_registered(&mut self, child_id: U, date: &NaiveDate) -> Result<bool> {
        self.persistence.is_registered(child_id, date)
            .map_err(|error| error.into())
    }

    pub fn unregister_child(&mut self, child_id: U, date: NaiveDate) -> Result<()> {
        self.persistence.unregister(child_id, date)
            .map_err(|error| error.into())
    }
}

impl<T, U> GenericService for Service<T, U> where
    T: Persistence<U>,
    U: ChildId
{
    type Request = Request<U>;
    type ServiceError = ServiceError;

    fn execute(&mut self, request: Self::Request) {
        use Request::*;

        match request {
            CreateChild(child, sender) => {
                let result = self.create_child(child);
                if let Some(_child_id) = self.send_result(result, sender) {
                    // TODO: remove new child
                };
            }
            GetChild(child_id, sender) => {
                let result = self.get_child(child_id);
                self.send_and_forget(result, sender);
            }
            GetAllChildren(sender) => {
                let result = self.get_all_children();
                self.send_and_forget(result, sender);
            }
            GetRegisteredChildren(date, sender) => {
                let result = self.get_registered_children(&date);
                self.send_and_forget(result, sender);
            }
            RegisterChild(child_id, date, sender) => {
                let result = self.register_child(child_id, date);
                self.send_and_forget(result, sender);
            }
            IsRegistered(child_id, date, sender) => {
                let result = self.is_registered(child_id, &date);
                self.send_and_forget(result, sender);
            }
            UnregisterChild(child_id, date, sender) => {
                let result = self.unregister_child(child_id, date);
                self.send_and_forget(result, sender);
            }
            Close => (),
        };
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::child::MemoryChildPersistence;

    impl<T, U> Service<T, U> where
        T: Persistence<U>,
        U: ChildId
    {
        pub fn with_child(&mut self) -> U {
            self.create_child(Child::any()).unwrap()
        }

        pub fn with_registered_child(&mut self) -> (U, NaiveDate) {
            let date = NaiveDate::from_ymd(2018, 1, 1);
            let child_id = self.create_child(Child::any()).unwrap();
            self.register_child(child_id, date.clone()).unwrap();
            (child_id, date)
        }
    }

    use chrono::Duration;

    fn get_service() -> Service<MemoryChildPersistence, u32> {
        Service::new(MemoryChildPersistence::new())
    }

    #[test]
    fn can_retrieve_created_child() {
        let mut service = get_service();
        let child = Child::any();

        let child_id = service.create_child(child.clone()).unwrap();

        let child_copy = service.get_child(child_id).unwrap();

        assert_eq!(child, child_copy);
    }

    #[test]
    fn register_unknown_id_return_child_not_found_error() {
        let mut service = get_service();
        let date = NaiveDate::from_ymd(2018, 1, 1);

        assert_eq!(ServiceError::ChildNotFound, service.register_child(0, date).unwrap_err());
    }

    #[test]
    fn can_register_created_child() {
        let mut service = get_service();
        let (child_id, start_date) = service.with_registered_child();

        let later_date = start_date + Duration::days(2);
        let before_date = start_date - Duration::days(2);

        assert!(service.is_registered(child_id, &later_date).unwrap());
        assert!(!service.is_registered(child_id, &before_date).unwrap());
    }

    #[test]
    fn can_unregister_after_registration_date() {
        let mut service = get_service();
        let (child_id, start_date) = service.with_registered_child();

        let unregister_date = start_date + Duration::days(2);

        service.unregister_child(child_id, unregister_date.clone()).unwrap();

        let after_unregister_date = unregister_date + Duration::days(2);

        assert!(!service.is_registered(child_id, &after_unregister_date).unwrap());
    }

    #[test]
    fn cannot_unregister_before_registration_date() {
        let mut service = get_service();
        let (child_id, start_date) = service.with_registered_child();

        let unregister_date = start_date - Duration::days(2);

        let error = service.unregister_child(child_id, unregister_date).unwrap_err();

        assert_eq!(error, ServiceError::NotRegistered);
    }

    #[test]
    fn can_register_after_being_unregistered() {
        let mut service = get_service();
        let (child_id, start_date) = service.with_registered_child();

        let unregister_date = start_date + Duration::days(2);

        service.unregister_child(child_id, unregister_date.clone()).unwrap();

        let register_again_date = unregister_date + Duration::days(2);

        service.register_child(child_id, register_again_date.clone()).unwrap();

        let after_register_again_date = register_again_date + Duration::days(2);

        assert!(service.is_registered(child_id, &after_register_again_date).unwrap());
    }
}
