use chrono::NaiveDate;

type Period = (NaiveDate, NaiveDate);

#[derive(Debug, Eq, PartialEq)]
pub enum RegisterError {
    AlreadyRegistered,
    CannotRegisterInThePast,
}

#[derive(Debug, Eq, PartialEq)]
pub enum UnregisterError {
    NotRegistered,
}

pub struct Registration {
    last_registered_date: Option<NaiveDate>,
    history: Vec<Period>,
}

impl Registration {
    pub fn new() -> Self {
        Self {
            last_registered_date: None,
            history: Vec::new(),
        }
    }

    pub fn register(&mut self, date: NaiveDate) -> Result<(), RegisterError> {
        match self.last_registered_date {
            Some(_) => Err(RegisterError::AlreadyRegistered),
            None => {
                if self.is_later_than_history(&date) {
                    self.last_registered_date.replace(date);
                    Ok(())
                } else {
                    Err(RegisterError::CannotRegisterInThePast)
                }
            },
        }
    }

    pub fn unregister(&mut self, date: NaiveDate) -> Result<(), UnregisterError> {
        match self.last_registered_date {
            Some(initial_date) => {
                if date <= initial_date {
                    Err(UnregisterError::NotRegistered)
                } else {
                    self.last_registered_date.take();
                    self.history.push((initial_date, date));
                    Ok(())
                }
            },
            None => Err(UnregisterError::NotRegistered),
        }
    }

    pub fn is_registered(&self, date: &NaiveDate) -> bool {
        if let Some(last_registered_date) = &self.last_registered_date {
            last_registered_date <= date
        } else {
            self.history.iter()
                .any(|(min_date, max_date)| min_date <= date && date <= max_date)
        }
    }

    fn is_later_than_history(&self, date: &NaiveDate) -> bool {
        self.history.is_empty() || self.history.iter()
            .any(|(_, max_date)| date > max_date)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn new_subject() -> Registration {
        Registration::new()
    }

    fn any_date() -> NaiveDate {
        NaiveDate::from_ymd(2017, 1, 1)
    }

    #[test]
    fn register_should_succeed_when_registering_for_the_first_time() {
        new_subject().register(any_date()).unwrap();
    }

    #[test]
    fn register_should_error_if_already_registered() {
        let mut subject = new_subject();
        let first_register_date = any_date();
        subject.register(first_register_date).unwrap();

        assert_eq!(
            RegisterError::AlreadyRegistered,
            subject.register(first_register_date.succ()).unwrap_err()
        );
    }

    #[test]
    fn register_should_error_if_trying_to_register_before_a_previous_period() {
        let mut subject = new_subject();
        let first_register_date = any_date();
        subject.register(first_register_date).unwrap();
        subject.unregister(first_register_date.succ()).unwrap();

        assert_eq!(
            RegisterError::CannotRegisterInThePast,
            subject.register(first_register_date.pred()).unwrap_err()
        );
        assert_eq!(false, subject.is_registered(&first_register_date.pred()))
    }

    #[test]
    fn unregister_should_error_if_not_registered() {
        let mut subject = new_subject();

        assert_eq!(
            UnregisterError::NotRegistered,
            subject.unregister(any_date()).unwrap_err()
        );
    }

    #[test]
    fn unregister_should_error_if_date_is_before_registration() {
        let mut subject = new_subject();
        let first_register_date = any_date();
        subject.register(first_register_date).unwrap();

        assert_eq!(
            UnregisterError::NotRegistered,
            subject.unregister(first_register_date.pred()).unwrap_err()
        );
    }

    #[test]
    fn new_registration_is_not_registered() {
        let subject = new_subject();

        assert_eq!(false, subject.is_registered(&any_date()))
    }

    #[test]
    fn is_registered_at_date_of_registration() {
        let mut subject = new_subject();
        let first_register_date = any_date();
        subject.register(first_register_date).unwrap();

        assert_eq!(true, subject.is_registered(&first_register_date))
    }

    #[test]
    fn is_registered_at_date_between_past_period() {
        let mut subject = new_subject();
        let first_register_date = any_date();
        subject.register(first_register_date).unwrap();

        assert_eq!(true, subject.is_registered(&first_register_date))
    }
}
