use crate::child::{
    Child,
    Persistence,
    PersistenceError,
    PersistenceResult,
    Registration,
    RegisterError,
    UnregisterError,
};

use chrono::NaiveDate;
use std::collections::HashMap;

pub struct MemoryChildPersistence {
    count: u32,
    children: HashMap<u32, Child>,
    registrations: HashMap<u32, Registration>,
}

impl MemoryChildPersistence {
    pub fn new() -> MemoryChildPersistence {
        MemoryChildPersistence {
            count: 0,
            children: HashMap::new(),
            registrations: HashMap::new(),
        }
    }

    pub fn get_new_id(&mut self) -> u32 {
        self.count += 1;
        self.count
    }
}

impl Into<PersistenceError> for RegisterError {
    fn into(self) -> PersistenceError {
        match self {
            Self::AlreadyRegistered => PersistenceError::AlreadyRegistered,
            Self::CannotRegisterInThePast => PersistenceError::CannotRegisterInThePast,
        }
    }
}

impl Into<PersistenceError> for UnregisterError {
    fn into(self) -> PersistenceError {
        match self {
            Self::NotRegistered => PersistenceError::NotRegistered,
        }
    }
}

impl Persistence<u32> for MemoryChildPersistence {
    fn create(&mut self, child: Child) -> PersistenceResult<u32> {
        let id = self.get_new_id();
        self.children.insert(id, child);
        self.registrations.insert(id, Registration::new());
        Ok(id)
    }

    fn get_child(&self, id: u32) -> PersistenceResult<Child> {
        self.children.get(&id)
            .cloned()
            .ok_or(PersistenceError::ChildNotFound)
    }

    fn get_all_children(&self) -> PersistenceResult<HashMap<u32, Child>> {
        Ok(self.children.clone())
    }

    fn register(&mut self, child_id: u32, date: NaiveDate) -> PersistenceResult<()> {
        self.registrations.get_mut(&child_id)
            .ok_or(PersistenceError::ChildNotFound)
            .and_then(|registration| registration.register(date)
                .map_err(|error| error.into()))
    }

    fn unregister(&mut self, child_id: u32, date: NaiveDate) -> PersistenceResult<()> {
        self.registrations.get_mut(&child_id)
            .ok_or(PersistenceError::ChildNotFound)
            .and_then(|registration| registration.unregister(date)
                .map_err(|error| error.into()))
    }

    fn get_registered_children(&self, date: &NaiveDate) -> PersistenceResult<Vec<u32>> {
        Ok(self.registrations.iter()
            .filter(|(_, registration)| registration.is_registered(date))
            .map(|(child_id, _)| *child_id)
            .collect())
    }

    fn is_registered(&self, child_id: u32, date: &NaiveDate) -> PersistenceResult<bool> {
        self.registrations.get(&child_id)
            .ok_or(PersistenceError::ChildNotFound)
            .map(|registration| registration.is_registered(date))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_subject() -> MemoryChildPersistence {
        MemoryChildPersistence::new()
    }

    fn get_child() -> Child {
        Child::any()
    }

    #[test]
    fn created_child_can_be_retrieved() {
        let mut subject = get_subject();
        let child = get_child();
        let id = subject.create(child.clone()).unwrap();

        assert_eq!(child, subject.get_child(id).unwrap());
    }

    #[test]
    fn get_child_should_throw_if_id_does_not_exist() {
        let subject = get_subject();
        let id = 4;

        assert_eq!(PersistenceError::ChildNotFound, subject.get_child(id).unwrap_err());
    }
}
