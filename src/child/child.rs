use chrono::NaiveDate;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Child {
    first_name: String,
    last_name: String,
    date_of_birth: NaiveDate,
}

impl Child {
    pub fn new(first_name: String, last_name: String, date_of_birth: NaiveDate) -> Self {
        Child {
            first_name,
            last_name,
            date_of_birth,
        }
    }

    #[cfg(test)]
    pub fn any() -> Self {
        Self::new("Robot".to_owned(), "Chicken".to_owned(), NaiveDate::from_ymd(2010, 2, 10))
    }

    pub fn first_name(&self) -> &String {
        &self.first_name
    }

    pub fn last_name(&self) -> &String {
        &self.last_name
    }

    pub fn date_of_birth(&self) -> &NaiveDate {
        &self.date_of_birth
    }
}
