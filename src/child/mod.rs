mod child;
mod child_id;
#[cfg(any(test, feature = "in_memory"))]
mod in_memory_persistence;
mod persistence;
#[cfg(any(test, feature = "in_memory"))]
mod registration;
mod request;
mod service;

pub use child_id::ChildId;
pub use child::Child;
pub use persistence::{Persistence, PersistenceError, Result as PersistenceResult};
#[cfg(any(test, feature = "in_memory"))]
pub use registration::{Registration, RegisterError, UnregisterError};
pub use request::Request;
pub use service::{
    Service,
    ServiceError,
    Result as ServiceResult,
};

#[cfg(any(test, feature = "in_memory"))]
pub use in_memory_persistence::MemoryChildPersistence;
