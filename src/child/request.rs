use crate::child::{Child, ServiceResult};
use crate::generic_service::ServiceRequest;

use chrono::NaiveDate;
use std::collections::HashMap;
use std::sync::mpsc::{channel, Receiver, Sender};

type ResultSender<T> = Sender<ServiceResult<T>>;
type ResultReceiver<T> = Receiver<ServiceResult<T>>;

#[derive(Clone)]
pub enum Request<ChildId: Send> {
    CreateChild(Child, ResultSender<ChildId>),
    GetChild(ChildId, ResultSender<Child>),
    GetAllChildren(ResultSender<HashMap<ChildId, Child>>),
    GetRegisteredChildren(NaiveDate, ResultSender<Vec<ChildId>>),
    RegisterChild(ChildId, NaiveDate, ResultSender<()>),
    IsRegistered(ChildId, NaiveDate, ResultSender<bool>),
    UnregisterChild(ChildId, NaiveDate, ResultSender<()>),
    Close,
}

impl<ChildId: Send> ServiceRequest for Request<ChildId> {
    fn close() -> Self {
        Self::Close
    }

    fn is_close(&self) -> bool {
        match self {
            Self::Close => true,
            _ => false,
        }
    }
}

impl<ChildId: Send> Request<ChildId> {
    pub fn create_child(child: Child) -> (Self, ResultReceiver<ChildId>) {
        let (sender, receiver) = channel();

        (Request::CreateChild(child, sender), receiver)
    }

    pub fn get_child(child_id: ChildId) -> (Self, ResultReceiver<Child>) {
        let (sender, receiver) = channel();

        (Request::GetChild(child_id, sender), receiver)
    }

    pub fn get_all_children() -> (Self, ResultReceiver<HashMap<ChildId, Child>>) {
        let (sender, receiver) = channel();

        (Request::GetAllChildren(sender), receiver)
    }

    pub fn get_registered_children(
        date: NaiveDate
    )-> (Self, ResultReceiver<Vec<ChildId>>) {
        let (sender, receiver) = channel();

        (Request::GetRegisteredChildren(date, sender), receiver)
    }

    pub fn register_child(
        child_id: ChildId,
        date: NaiveDate,
    ) -> (Self, ResultReceiver<()>) {
        let (sender, receiver) = channel();

        (Request::RegisterChild(child_id, date, sender), receiver)
    }

    pub fn is_registered(
        child_id: ChildId,
        date: NaiveDate,
    ) -> (Self, ResultReceiver<bool>) {
        let (sender, receiver) = channel();

        (Request::IsRegistered(child_id, date, sender), receiver)
    }

    pub fn unregister_child(
        child_id: ChildId,
        date: NaiveDate,
    ) -> (Self, ResultReceiver<()>) {
        let (sender, receiver) = channel();

        (Request::UnregisterChild(child_id, date, sender), receiver)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    type Request = super::Request<u32>;

    #[test]
    fn is_close_is_true_for_close_provider() {
        assert!(Request::close().is_close());
    }
}
