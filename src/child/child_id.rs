use std::hash::Hash;

pub trait ChildId: Copy + Eq + Hash + Send {}

#[cfg(any(test, feature = "in_memory"))]
impl ChildId for u32 {}
