use std::ops;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Money {
    cents: i32,
}

impl Money {
    pub fn from_cents(cents: i32) -> Self {
        Self {
            cents
        }
    }

    pub fn from_dollars(dollars: i32) -> Self {
        Self {
            cents: dollars * 100,
        }
    }

    pub fn zero() -> Self {
        Self::from_cents(0)
    }

    pub fn into_cents(&self) -> i32 {
        self.cents
    }

    pub fn into_dollars(&self) -> f64 {
        self.cents as f64 / 100f64
    }
}

impl ops::Add<Money> for Money {
    type Output = Money;

    fn add(self, rhs: Money) -> Money {
        Money::from_cents(self.cents + rhs.cents)
    }
}

impl ops::AddAssign<Money> for Money {
    fn add_assign(&mut self, rhs: Money) {
        self.cents += rhs.cents;
    }
}

impl ops::Sub<Money> for Money {
    type Output = Money;

    fn sub(self, rhs: Money) -> Money {
        Money::from_cents(self.into_cents() - rhs.into_cents())
    }
}

impl ops::SubAssign<Money> for Money {
    fn sub_assign(&mut self, rhs: Money) {
        self.cents -= rhs.cents;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_dollar_amount_has_the_correct_amount_of_cents() {
        let dollar_amount = 7;

        assert_eq!(dollar_amount * 100, Money::from_dollars(dollar_amount).into_cents());
    }

    #[test]
    fn from_cent_amount_has_the_correct_amount_of_cents() {
        let cent_amount = 189;

        assert_eq!(cent_amount, Money::from_cents(cent_amount).into_cents());
    }

    #[test]
    fn add_two_moneys_has_correct_result() {
        let cent_amount_a = 200;
        let cent_amount_b = 100;

        assert_eq!(
            Money::from_cents(cent_amount_a + cent_amount_b),
            Money::from_cents(cent_amount_a) + Money::from_cents(cent_amount_b)
        );
    }

    #[test]
    fn subtract_two_moneys_has_correct_result() {
        let cent_amount_a = 200;
        let cent_amount_b = 100;

        assert_eq!(
            Money::from_cents(cent_amount_a - cent_amount_b),
            Money::from_cents(cent_amount_a) - Money::from_cents(cent_amount_b)
        );
    }
}
