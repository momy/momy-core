use crate::billing::{Pricing, ServiceResult};
use crate::generic_service::ServiceRequest;

use chrono::NaiveDate;
use std::sync::mpsc::{channel, Receiver, Sender};

type ResultSender<T> = Sender<ServiceResult<T>>;
type ResultReceiver<T> = Receiver<ServiceResult<T>>;

#[derive(Clone)]
pub enum Request {
    GetPricing(NaiveDate, ResultSender<Pricing>),
    SetPricing(NaiveDate, Pricing, ResultSender<()>),
    Close,
}

impl Request {
    pub fn get_pricing(date: NaiveDate) -> (Self, ResultReceiver<Pricing>) {
        let (sender, receiver) = channel();

        (Self::GetPricing(date, sender), receiver)
    }

    pub fn set_pricing(date: NaiveDate, pricing: Pricing) -> (Self, ResultReceiver<()>) {
        let (sender, receiver) = channel();

        (Self::SetPricing(date, pricing, sender), receiver)
    }
}

impl ServiceRequest for Request {
    fn close() -> Self {
        Self::Close
    }

    fn is_close(&self) -> bool {
        match self {
            Self::Close => true,
            _ => false,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn is_close_is_true_for_close_provider() {
        assert!(Request::close().is_close());
    }
}
