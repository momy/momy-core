use crate::billing::Pricing;

use chrono::NaiveDate;

pub enum PersistenceError {
    NoPricingAtDate(NaiveDate),
    InternalError,
}

pub type Result<T> = std::result::Result<T, PersistenceError>;

pub trait Persistence {
    fn add_new_pricing(&mut self, pricing: Pricing, date: NaiveDate) -> Result<()>;
    fn get_pricing(&self, date: &NaiveDate) -> Result<Pricing>;
    fn get_last_pricing_changed_date(&self) -> Result<Option<NaiveDate>>;
}
