use crate::billing::{Persistence, PersistenceError};
use crate::billing::Pricing;
use crate::sorted_by_date_vector::SortedByDateVector;

use chrono::NaiveDate;

type Result<T> = std::result::Result<T, PersistenceError>;

pub struct MemoryBillingPersistence {
    pricings: SortedByDateVector<Pricing>,
}

impl MemoryBillingPersistence {
    pub fn new(first_pricing: Pricing, date: NaiveDate) -> Self {
        Self {
            pricings: SortedByDateVector::new(first_pricing, date),
        }
    }
}

impl Persistence for MemoryBillingPersistence {
    fn add_new_pricing(&mut self, pricing: Pricing, date: NaiveDate) -> Result<()> {
        self.pricings.insert(pricing, date);
        Ok(())
    }

    fn get_pricing(&self, date: &NaiveDate) -> Result<Pricing> {
        self.pricings.get_from_date(date)
            .cloned()
            .ok_or_else(|| PersistenceError::NoPricingAtDate(date.clone()))
    }

    fn get_last_pricing_changed_date(&self) -> Result<Option<NaiveDate>> {
        Ok(Some(self.pricings.last().1.clone()))
    }
}
