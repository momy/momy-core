use crate::billing::Money;
use crate::attendance::{ExpectedAttendance, FullDayAttendance, SchoolDayAttendance};

use chrono::NaiveDate;
use std::convert::TryInto;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Pricing {
    school_am: Money,
    school_lunch: Money,
    school_pm: Money,
    missed_school_am: Money,
    missed_school_lunch: Money,
    missed_school_pm: Money,
    overtime_per_hour: Money,
    overtime_minute_slice: u32,
    full_day_price: Money,
    missed_full_day: Money
}

impl Pricing {
    pub fn new(
        school_am: Money,
        school_lunch: Money,
        school_pm: Money,
        missed_school_am: Money,
        missed_school_lunch: Money,
        missed_school_pm: Money,
        overtime_per_hour: Money,
        overtime_minute_slice: u32,
        full_day_price: Money,
        missed_full_day: Money
    ) -> Self {
        Self {
            school_am,
            school_lunch,
            school_pm,
            missed_school_am,
            missed_school_lunch,
            missed_school_pm,
            overtime_per_hour,
            overtime_minute_slice,
            full_day_price,
            missed_full_day,
        }
    }

    pub fn from_cents(
        school_am: i32,
        school_lunch: i32,
        school_pm: i32,
        missed_school_am: i32,
        missed_school_lunch: i32,
        missed_school_pm: i32,
        overtime_per_hour: i32,
        overtime_minute_slice: u32,
        full_day: i32,
        missed_full_day: i32,
    ) -> Self {
        Self {
            school_am: Money::from_cents(school_am),
            school_lunch: Money::from_cents(school_lunch),
            school_pm: Money::from_cents(school_pm),
            missed_school_am: Money::from_cents(missed_school_am),
            missed_school_lunch: Money::from_cents(missed_school_lunch),
            missed_school_pm: Money::from_cents(missed_school_pm),
            overtime_per_hour: Money::from_cents(overtime_per_hour),
            overtime_minute_slice,
            full_day_price: Money::from_cents(full_day),
            missed_full_day: Money::from_cents(missed_full_day),
        }
    }

    #[cfg(test)]
    pub fn any() -> Self {
        Self {
            school_am: Money::from_cents(200),
            school_lunch: Money::from_cents(350),
            school_pm: Money::from_cents(650),
            missed_school_am: Money::from_cents(100),
            missed_school_lunch: Money::from_cents(175),
            missed_school_pm: Money::from_cents(325),
            overtime_per_hour: Money::from_cents(2000),
            overtime_minute_slice: 15,
            full_day_price: Money::from_cents(2500),
            missed_full_day: Money::from_cents(1250),
        }
    }

    pub fn calculate_school_day(
        &self,
        attendance: &SchoolDayAttendance,
        expected: &ExpectedAttendance
    ) -> Money {
        match expected {
            ExpectedAttendance::Absent => {
                attendance.if_present_am(self.school_am, Money::zero())
                + attendance.if_present_lunch(self.school_lunch, Money::zero())
                + attendance.if_present_pm(self.school_pm, Money::zero())
            },
            ExpectedAttendance::SchoolDay(expected) => {
                attendance.if_present_am(
                    self.school_am,
                    expected.if_present_am(self.missed_school_am, Money::zero())
                )
                + attendance.if_present_lunch(
                    self.school_lunch,
                    expected.if_present_lunch(self.missed_school_lunch, Money::zero())
                )
                + attendance.if_present_pm(
                    self.school_pm,
                    expected.if_present_pm(self.missed_school_pm, Money::zero())
                )
            },
            ExpectedAttendance::FullDay => {
                attendance.if_present_am(self.school_am, Money::zero())
                + attendance.if_present_lunch(self.school_lunch, Money::zero())
                + attendance.if_present_pm(self.school_pm, Money::zero())
            }
        }
    }

    pub fn calculate_full_day(
        &self,
        attendance: &FullDayAttendance,
        expected: &ExpectedAttendance
    ) -> Money {
        match expected {
            ExpectedAttendance::Absent => {
                attendance.if_present(self.full_day_price, Money::zero())
            },
            ExpectedAttendance::SchoolDay(_) => {
                attendance.if_present(self.full_day_price, Money::zero())
            },
            ExpectedAttendance::FullDay => {
                attendance.if_present(self.full_day_price, self.missed_full_day)
            }
        }
    }

    pub fn overtime(&self, minutes: u32) -> Money {
        if minutes == 0 {
            Money::zero()
        } else {
            let mut slices: i32 = (minutes / self.overtime_minute_slice).try_into().unwrap();
            let remainder = minutes % self.overtime_minute_slice;

            if remainder != 0 {
                slices += 1;
            }
            let slice_minutes: i32 = self.overtime_minute_slice.try_into().unwrap();

            Money::from_cents(slices * slice_minutes * self.overtime_per_hour.into_cents() / 60)
        }
    }

    pub fn school_am(&self) -> Money {
        self.school_am
    }

    pub fn school_lunch(&self) -> Money {
        self.school_lunch
    }

    pub fn school_pm(&self) -> Money {
        self.school_pm
    }

    pub fn missed_school_am(&self) -> Money {
        self.missed_school_am
    }

    pub fn missed_school_lunch(&self) -> Money {
        self.missed_school_lunch
    }

    pub fn missed_school_pm(&self) -> Money {
        self.missed_school_pm
    }

    pub fn full_day(&self) -> Money {
        self.full_day_price
    }

    pub fn missed_full_day(&self) -> Money {
        self.missed_full_day
    }

    pub fn overtime_per_hour(&self) -> Money {
        self.overtime_per_hour
    }

    pub fn overtime_minute_slice(&self) -> u32 {
        self.overtime_minute_slice
    }

    pub fn with_school_am(mut self, amount: Money) -> Self {
        self.school_am = amount;
        self
    }
}

pub struct PricingPeriod {
    pricing: Pricing,
    start_date: NaiveDate,
}

impl PricingPeriod {
    pub fn new(pricing: Pricing, start_date: NaiveDate) -> PricingPeriod {
        PricingPeriod {
            pricing,
            start_date,
        }
    }

    pub fn get_start_date(&self) -> &NaiveDate {
        &self.start_date
    }

    pub fn get_pricing(&self) -> &Pricing {
        &self.pricing
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    lazy_static! {
    static ref SCHOOL_AM: Money = Money::from_cents(200);
    static ref SCHOOL_LUNCH: Money = Money::from_cents(350);
    static ref SCHOOL_PM: Money = Money::from_cents(650);
    static ref MISSED_SCHOOL_AM: Money = Money::from_cents(100);
    static ref MISSED_SCHOOL_LUNCH: Money = Money::from_cents(175);
    static ref MISSED_SCHOOL_PM: Money = Money::from_cents(325);
    static ref OVERTIME_PER_HOUR: Money = Money::from_cents(2000);
    static ref FULL_DAY: Money = Money::from_cents(2500);
    static ref MISSED_FULL_DAY: Money = Money::from_cents(1250);
    }
    static OVERTIME_MINUTE_SLICE: u32 = 15;

    fn get_pricing() -> Pricing {
        Pricing {
            school_am: *SCHOOL_AM,
            school_lunch: *SCHOOL_LUNCH,
            school_pm: *SCHOOL_PM,
            missed_school_am: *MISSED_SCHOOL_AM,
            missed_school_lunch: *MISSED_SCHOOL_LUNCH,
            missed_school_pm: *MISSED_SCHOOL_PM,
            overtime_per_hour: *OVERTIME_PER_HOUR,
            overtime_minute_slice: OVERTIME_MINUTE_SLICE,
            full_day_price: *FULL_DAY,
            missed_full_day: *MISSED_FULL_DAY,
        }
    }

    fn get_expected_school(am: bool, lunch: bool, pm: bool) -> ExpectedAttendance {
        ExpectedAttendance::SchoolDay(SchoolDayAttendance::new(am, lunch, pm))
    }

    mod school_day {
        use super::*;

        #[test]
        fn absent_school_attendance_should_cost_none_if_expected() {
            let attendance = SchoolDayAttendance::absent();
            let expected = ExpectedAttendance::Absent;

            assert_eq!(
                get_pricing().calculate_school_day(&attendance, &expected),
                Money::zero()
            );
        }

        #[test]
        fn absent_school_attendance_should_cost_if_not_expected() {
            let attendance = SchoolDayAttendance::absent();

            assert_eq!(
                get_pricing().calculate_school_day(
                    &attendance,
                    &get_expected_school(true, false, false)
                ),
                *MISSED_SCHOOL_AM
            );

            assert_eq!(
                get_pricing().calculate_school_day(
                    &attendance,
                    &get_expected_school(false, true, false)
                ),
                *MISSED_SCHOOL_LUNCH
            );

            assert_eq!(
                get_pricing().calculate_school_day(
                    &attendance,
                    &get_expected_school(false, false, true)
                ),
                *MISSED_SCHOOL_PM
            );

            assert_eq!(
                get_pricing().calculate_school_day(
                    &attendance,
                    &get_expected_school(true, true, false)
                ),
                *MISSED_SCHOOL_AM + *MISSED_SCHOOL_LUNCH
            );

            assert_eq!(
                get_pricing().calculate_school_day(
                    &attendance,
                    &get_expected_school(true, true, true)
                ),
                *MISSED_SCHOOL_AM + *MISSED_SCHOOL_LUNCH + *MISSED_SCHOOL_PM
            );
        }
    }

    mod full_day {
        use super::*;

        #[test]
        fn attend_full_day_cost_full_day() {
            let attendance = FullDayAttendance::new(true);
            let expected = ExpectedAttendance::FullDay;

            assert_eq!(
                get_pricing().calculate_full_day(&attendance, &expected),
                *FULL_DAY
            )
        }

        #[test]
        fn missed_full_day_cost_correct_price() {
            let attendance = FullDayAttendance::new(false);
            let expected = ExpectedAttendance::FullDay;

            assert_eq!(
                get_pricing().calculate_full_day(&attendance, &expected),
                *MISSED_FULL_DAY
            )
        }
    }

    mod overtime {
        use super::*;

        #[test]
        fn zero_minutes_should_not_cost_overtime() {
            assert_eq!(
                get_pricing().overtime(0),
                Money::zero()
            );
        }

        #[test]
        fn less_than_time_slice_should_cost_one_time_slice() {
            let expected_price = Money::from_cents(
                OVERTIME_PER_HOUR.into_cents() * (OVERTIME_MINUTE_SLICE as i32) / 60
            );
            assert_eq!(
                get_pricing().overtime(OVERTIME_MINUTE_SLICE - 1),
                expected_price
            );
        }

        #[test]
        fn exact_time_slice_should_cost_one_time_slice() {
            let expected_price = Money::from_cents(
                OVERTIME_PER_HOUR.into_cents() * (OVERTIME_MINUTE_SLICE as i32) / 60
            );
            assert_eq!(
                get_pricing().overtime(OVERTIME_MINUTE_SLICE),
                expected_price
            );
        }

        #[test]
        fn between_one_and_two_time_slice_should_cost_two_time_slices() {
            let expected_price = Money::from_cents(
                2 * OVERTIME_PER_HOUR.into_cents() * (OVERTIME_MINUTE_SLICE as i32) / 60
            );
            assert_eq!(
                get_pricing().overtime(OVERTIME_MINUTE_SLICE + 1),
                expected_price
            );
        }
    }
}
