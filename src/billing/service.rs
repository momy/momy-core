use crate::attendance::{Attendance, ExpectedAttendance};
use crate::billing::{
    Bill,
    Persistence,
    PersistenceError,
    Request,
    Pricing,
};
use crate::generic_service::Service as  GenericService;

use chrono::NaiveDate;

#[derive(Debug, Eq, PartialEq)]
pub enum ServiceError {
    NoPricingAtDate(NaiveDate),
    CannotChangePricingBeforeDate(NaiveDate),
    PersistenceError,
}

pub type Result<T> = std::result::Result<T, ServiceError>;

pub struct Service<T: Persistence> {
    persistence: T,
}

impl From<PersistenceError> for ServiceError {
    fn from(error: PersistenceError) -> ServiceError {
        match error {
            PersistenceError::NoPricingAtDate(date) => ServiceError::NoPricingAtDate(date),
            PersistenceError::InternalError => ServiceError::PersistenceError,
        }
    }
}

fn into_error(error: PersistenceError) -> ServiceError {
    error.into()
}

impl<T: Persistence> Service<T> {
    pub fn new(persistence: T) -> Self {
        Self {
            persistence,
        }
    }

    pub fn get_pricing(&self, date: &NaiveDate) -> Result<Pricing> {
        self.persistence.get_pricing(&date)
            .map_err(into_error)
    }

    pub fn set_pricing(&mut self, date: NaiveDate, pricing: Pricing) -> Result<()> {
        let last_pricing_date = self.persistence.get_last_pricing_changed_date()?
            .filter(|last_date| &date < last_date);

        if let Some(last_pricing_date) = last_pricing_date {
            Err(ServiceError::CannotChangePricingBeforeDate(last_pricing_date.clone()))
        } else {
            self.persistence.add_new_pricing(pricing, date)
                .map_err(into_error)
        }
    }

    pub fn calculate_day_bill(
        &self,
        date: &NaiveDate,
        attendance: &Attendance,
        expected_attendance: &ExpectedAttendance
    ) -> Result<Bill> {
        self.persistence.get_pricing(date)
            .map(|pricing| {
                match attendance {
                    Attendance::SchoolDay(school_attendance, overtime_minutes) => Bill::new()
                        .with_school_day(pricing.calculate_school_day(school_attendance, expected_attendance))
                        .with_overtime(pricing.overtime(*overtime_minutes)),
                    Attendance::FullDay(full_day_attendance, overtime_minutes) => Bill::new()
                        .with_full_day(pricing.calculate_full_day(full_day_attendance, expected_attendance))
                        .with_overtime(pricing.overtime(*overtime_minutes)),
                }
            })
            .map_err(|error| error.into())
    }
}

impl<T: Persistence> GenericService for Service<T> {
    type Request = Request;
    type ServiceError = ServiceError;

    fn execute(&mut self, request: Self::Request) {
        match request {
            Request::GetPricing(date, sender) => {
                let result = self.get_pricing(&date);
                self.send_and_forget(result, sender);
            },
            Request::SetPricing(date, pricing, sender) => {
                let result = self.set_pricing(date, pricing);
                self.send_and_forget(result, sender);
            },
            Request::Close => (),
        };
    }
}

#[cfg(test)]
mod tests {
    extern crate chrono;
    use super::*;
    use crate::billing::{MemoryBillingPersistence, Money, Pricing};

    use chrono::{NaiveDate, Duration};

    lazy_static! {
    static ref START_DATE: NaiveDate = NaiveDate::from_ymd(2017, 1, 1);
    static ref BEFORE_START_DATE: NaiveDate = *START_DATE + Duration::days(-2);
    static ref AFTER_START_DATE: NaiveDate = *START_DATE + Duration::days(2);
    }

    fn get_pricing() -> Pricing {
        Pricing::any()
    }

    fn subject() -> Service<MemoryBillingPersistence> {
        Service::new(MemoryBillingPersistence::new(get_pricing(), *START_DATE))
    }

    #[test]
    fn calculate_day_bill_return_no_price_at_date_if_date_is_before_first_date() {
        let result = subject()
            .calculate_day_bill(&BEFORE_START_DATE, &Attendance::any(), &ExpectedAttendance::any())
            .unwrap_err();

        assert_eq!(result, ServiceError::NoPricingAtDate(*BEFORE_START_DATE));
    }

    #[test]
    fn get_pricing_after_start_date_returns_pricing() {
        let result = subject()
            .get_pricing(&AFTER_START_DATE)
            .unwrap();

        assert_eq!(result, get_pricing());
    }

    #[test]
    fn get_pricing_before_start_date_returns_no_price_at_date() {
        let result = subject()
            .get_pricing(&BEFORE_START_DATE)
            .unwrap_err();

        assert_eq!(result, ServiceError::NoPricingAtDate(*BEFORE_START_DATE));
    }

    #[test]
    fn add_new_pricing_then_get_pricing_after_returns_added_pricing() {
        let later_date = *AFTER_START_DATE + Duration::days(2);
        let new_pricing = Pricing::any().with_school_am(Money::from_dollars(10));

        let mut service = subject();
        service.set_pricing(*AFTER_START_DATE, new_pricing.clone()).unwrap();

        let result = service.get_pricing(&later_date).unwrap();

        assert_eq!(result, new_pricing);
    }

    #[test]
    fn add_new_pricing_then_get_pricing_before_returns_previous_pricing() {
        let later_date = *AFTER_START_DATE + Duration::days(2);
        let new_pricing = Pricing::any().with_school_am(Money::from_dollars(10));

        let mut service = subject();
        service.set_pricing(later_date, new_pricing.clone()).unwrap();

        let result = service.get_pricing(&AFTER_START_DATE).unwrap();

        assert_eq!(result, get_pricing());
    }

    #[test]
    fn add_new_pricing_before_last_changed_date_returns_error() {
        let later_date = *AFTER_START_DATE + Duration::days(2);
        let new_pricing = Pricing::any().with_school_am(Money::from_dollars(10));

        let mut service = subject();
        service.set_pricing(later_date, new_pricing.clone()).unwrap();

        let result = service.set_pricing(*AFTER_START_DATE, new_pricing).unwrap_err();

        assert_eq!(result, ServiceError::CannotChangePricingBeforeDate(later_date));
    }
}
