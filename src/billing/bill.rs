use crate::billing::Money;

#[derive(Debug, Eq, PartialEq)]
pub struct Bill {
    total: Money,
}

impl Bill {
    pub fn new() -> Self {
        Self {
            total: Money::from_cents(0),
        }
    }

    pub fn with_overtime(mut self, amount: Money) -> Self {
        self.total += amount;
        self
    }

    pub fn with_school_day(mut self, amount: Money) -> Self {
        self.total += amount;
        self
    }

    pub fn with_full_day(mut self, amount: Money) -> Self {
        self.total += amount;
        self
    }
}
