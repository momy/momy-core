#[macro_use]
extern crate lazy_static;

mod day_care;

pub mod attendance;
pub mod billing;
pub mod child;
pub mod generic_service;

#[cfg(any(test, feature = "in_memory"))]
pub(crate) mod sorted_by_date_vector;

pub use day_care::{DayCare, DayCareError};
