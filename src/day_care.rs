use crate::attendance;
use crate::billing;
use crate::child;
use crate::generic_service::{ServiceHandle, spawn_service, RequestSender};

#[derive(Debug)]
pub enum DayCareError {
    ServiceInitialisation,
}

pub struct DayCare<T, U, V, W> where
    T: attendance::Persistence<W>,
    U: billing::Persistence,
    V: child::Persistence<W>,
    W: child::ChildId,
{
    attendance_handle: ServiceHandle<attendance::Service<T, W>>,
    billing_handle: ServiceHandle<billing::Service<U>>,
    child_handle: ServiceHandle<child::Service<V, W>>,
}

impl<T, U, V, W> DayCare<T, U, V, W> where
    T: attendance::Persistence<W> + Send + 'static,
    U: billing::Persistence + Send + 'static,
    V: child::Persistence<W> + Send + 'static,
    W: child::ChildId + 'static,
{
    pub fn new(
        attendance_persistence: T,
        billing_persistence: U,
        child_persistence: V
    ) -> Result<Self, DayCareError> {
        let attendance_service = attendance::Service::new(attendance_persistence);
        let billing_service = billing::Service::new(billing_persistence);
        let child_service = child::Service::new(child_persistence);

        let attendance_handle = spawn_service(attendance_service);
        let billing_handle = spawn_service(billing_service);
        let child_handle = spawn_service(child_service);

        attendance_handle.get_sender().send_request(
            attendance::Request::set_child_request_sender(child_handle.get_sender().clone()))
                .map_err(|_| DayCareError::ServiceInitialisation)?;

        Ok(DayCare {
            attendance_handle,
            billing_handle,
            child_handle,
        })
    }

    pub fn attendance_sender(&self) -> &RequestSender<attendance::Request<W>> {
        self.attendance_handle.get_sender()
    }

    pub fn billing_sender(&self) -> &RequestSender<billing::Request> {
        self.billing_handle.get_sender()
    }

    pub fn child_sender(&self) -> &RequestSender<child::Request<W>> {
        self.child_handle.get_sender()
    }

    pub fn join(&mut self) {
        self.attendance_handle.join();
        self.billing_handle.join();
        self.child_handle.join();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::child;
    use crate::attendance::MemoryAttendancePersistence;
    use crate::billing::{MemoryBillingPersistence, Pricing};
    use crate::child::{Child, MemoryChildPersistence};

    use chrono::NaiveDate;

    fn new_day_care() -> DayCare<MemoryAttendancePersistence, MemoryBillingPersistence, MemoryChildPersistence, u32> {
        DayCare::new(
            MemoryAttendancePersistence::new(),
            MemoryBillingPersistence::new(Pricing::any(), NaiveDate::from_ymd(2018, 1, 1)),
            MemoryChildPersistence::new(),
        ).unwrap()
    }

    #[test]
    fn join_should_finish() {
        new_day_care().join();
    }

    #[test]
    fn get_new_registered_child() {
        let child = Child::any();
        let (request, receiver) = child::Request::create_child(child.clone());
        let mut day_care = new_day_care();

        let sender = day_care.child_sender();
        let child_id = sender.send_request_with_result(request, receiver).unwrap();

        let (get_request, get_receiver) = child::Request::get_child(child_id);
        let child_copy = sender.send_request_with_result(get_request, get_receiver).unwrap();

        day_care.join();

        assert_eq!(child, child_copy);
    }
}
